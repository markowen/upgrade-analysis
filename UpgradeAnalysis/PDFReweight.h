#include "LHAPDF/LHAPDF.h"

enum PDFSets {
  NNPDF31,
  CT14,
  MMHT2014
};

/**
 * Small class to perform PDF reweighting.
 *
 * To use in the code, simply add an object of type PDFReweight to your code.
 * Then in the event loop, you can call:
 * getReweightingFactor(x1, id1, x2, id2, q, PDFinfo_XF1, PDFinfo_XF2, pdfset);
 * where:
 *  x1, id2, x2, id2 are the x and pdg IDs for the two incoming partons
 *  q is the Q value of the hard-process
 *  PDFinfo_XF1 and PDFinfo_XF2 are the values of the PDF for partons 1 and stored in input file (used to make a consistency check)
 *  pdfset controls which PDF set you reweight to, see the enum above for the possible options
 */
class PDFReweight {

 public:
  PDFReweight();
  ~PDFReweight();

  // reweight like w= (PDF(x1,f1,Q) * PDF(x2,f2,Q)) / ((PDF_0(x1,f1,Q) * PDF_0(x2,f2,Q))
  double getReweightingFactor(double x1, int id1, double x2, int id2, double q, double PDFinfo_XF1, double PDFinfo_XF2, PDFSets pdfset);

 protected:
  /// Nominal PDF set. this is set to NNPDF 3.0 (used for ttbar sample)
  LHAPDF::PDF* m_nompdf = 0;

  /// NNPDF 3.1 PDF set
  LHAPDF::PDF* m_nnpdf = 0;

  /// CT14 PDF set
  LHAPDF::PDF* m_ct14pdf = 0;

  /// MMHT 2014 PDF set
  LHAPDF::PDF* m_mmhtpdf = 0;
};
