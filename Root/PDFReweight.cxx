#include "UpgradeAnalysis/PDFReweight.h"

using namespace LHAPDF;

PDFReweight::PDFReweight() {
  // hack to get access to latest PDF sets
  pathsPrepend("/cvmfs/atlas.cern.ch/repo/sw/Generators/lhapdfsets/current");

  m_nompdf = mkPDF("NNPDF30_nlo_as_0118",0);
  m_nnpdf = mkPDF("NNPDF31_nlo_pdfas",0);
  m_ct14pdf = mkPDF("CT14nlo",0);
  m_mmhtpdf = mkPDF("MMHT2014nlo68cl",0);
}

PDFReweight::~PDFReweight() {

}

// we need to reweight like w= (PDF(x1,f1,Q) * PDF(x2,f2,Q)) / ((PDF_0(x1,f1,Q) * PDF_0(x2,f2,Q))
// where PDF is the PDF to reweight to and PDF_0 is the nominal PDF used for the MC generation
double PDFReweight::getReweightingFactor(double x1, int id1, double x2, int id2, double q, double PDFinfo_XF1, double PDFinfo_XF2, PDFSets pdfset) {

  LHAPDF::PDF* pdf = 0;
  switch(pdfset) {
  case NNPDF31:
    pdf = m_nnpdf;
    break;
  case CT14:
    pdf = m_ct14pdf;
    break;
  case MMHT2014:
    pdf = m_mmhtpdf;
    break;
  default:
    std::cerr << "ERROR: getReweightingFactor not setup for this pdf set " << ((int)pdfset) << std::endl;
    return 1.0;
  }

  // Get nominal PDF values and check they match input from ntuple
  double nom_xf1 = m_nompdf->xfxQ(id1, x1, q);
  double nom_xf2 = m_nompdf->xfxQ(id2, x2, q);
  if(fabs(nom_xf1 - PDFinfo_XF1)/nom_xf1 > 0.01) {
    std::cout << "WARNING: inconsistent XF1 with input MC " << nom_xf1 << " " << PDFinfo_XF1 << std::endl;
  }
  if(fabs(nom_xf2 - PDFinfo_XF2)/nom_xf2 > 0.01) {
    std::cout << "WARNING: inconsistent XF2 with input MC " << nom_xf2 << " " << PDFinfo_XF2 << std::endl;
  }
  double nom_w = nom_xf1*nom_xf2;

  double nnpdf_w = pdf->xfxQ(id1, x1, q) * pdf->xfxQ(id2, x2, q);

  //std::cout << nom_w << " " << nnpdf_w << std::endl;

  return nnpdf_w / nom_w;
}
