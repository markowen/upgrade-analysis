#include "UpgradeAnalysis/UpgradeTree.h"

UpgradeTree::UpgradeTree() :
  myrand()
{

}

UpgradeTree::~UpgradeTree() {
  fout->Write();
  fout->Close();
}

void UpgradeTree::CreateHistograms() {
  fout = TFile::Open("UpgradeOutput.root","RECREATE");
  fout->cd();
  this->DefineHistograms();
}

void UpgradeTree::LoadObjects(bool loadReco) {
  // Load reco objects
  if(loadReco) {
    // leptons
    tlv_leptons.resize( mu_pt.GetSize() + el_pt.GetSize() );
    for(unsigned int imu=0; imu<mu_pt.GetSize(); ++imu) {
      tlv_leptons[imu].SetPtEtaPhiE( mu_pt[imu], mu_eta[imu], mu_phi[imu], mu_e[imu] );
    }// muon loop
    for(unsigned int iel=0; iel<el_pt.GetSize(); ++iel) {
      tlv_leptons[mu_pt.GetSize() + iel].SetPtEtaPhiE( el_pt[iel], el_eta[iel], el_phi[iel], el_e[iel] );        
    }// electron loop

    // Jets
    tlv_jets.resize( jet_pt.GetSize() );
    jet_istagged.resize( jet_pt.GetSize() );
    for(unsigned int ij=0; ij<jet_pt.GetSize(); ++ij) {
      tlv_jets[ij].SetPtEtaPhiE( jet_pt[ij], jet_eta[ij], jet_phi[ij], jet_e[ij] );
      jet_istagged[ij] = (myrand.Uniform() < jet_mv1eff[ij]);
    }
    // MET
    metx = (*met_met) * cos(*met_phi);
    mety = (*met_met) * sin(*met_phi);
    
  }//loadReco

  // load truth objects
  tlv_truth_t.SetPtEtaPhiM (*MC_t_afterFSR_pt, *MC_t_afterFSR_eta, *MC_t_afterFSR_phi, *MC_t_afterFSR_m );
  tlv_truth_tbar.SetPtEtaPhiM (*MC_tbar_afterFSR_pt, *MC_tbar_afterFSR_eta, *MC_tbar_afterFSR_phi, *MC_tbar_afterFSR_m );
}

void UpgradeTree::SetTrees(TTree* upgradeTree, TTree* truthTree) {
  // build index needed to correctly read correct event from the upgrade tree
  upgradeTree->BuildIndex("runNumber","eventNumber");
  // pass trees to readers
  fReaderUpgrade.SetTree(upgradeTree);
  fReaderTruth.SetTree(truthTree);
}

void UpgradeTree::LoopOnCurrentTrees() {
  while( fReaderTruth.Next() ) {
    // Load MC tree entry
    Long64_t upentry = fReaderUpgrade.GetTree()->GetEntryNumberWithIndex( *runNumber, *eventNumber );
    //std::cout << *eventNumber << " " << *runNumber << " Got MC entry " << mcentry << std::endl;
    const bool processReco = upentry > -1;
    if(processReco) fReaderUpgrade.SetEntry( upentry );

    // Load reco objects into TLorentzVectors
    this->LoadObjects(processReco);

    // Process analysis code
    this->ProcessEvent(processReco);
  }

}

