#ifndef UPGRADEANALYSIS_UPGRADELOOP_H__
#define UPGRADEANALYSIS_UPGRADELOOP_H__

#include <vector>
#include <string>

#include "UpgradeTree.h"

class UpgradeLoop {

 public:
  UpgradeLoop(const std::vector<std::string>& filesToProc);
  ~UpgradeLoop();

  void SetAnalysisObject( UpgradeTree* anaobj );

  int RunLoop();

 protected:
  
  std::vector<std::string> m_filesToProc;
  
  UpgradeTree* m_upgradeTree;

  // TTreeReader for weights tree
  TTreeReader m_weightsTreeReader;

  // some reader variables to read the weights tree in the files
  TTreeReaderValue<Float_t> totalEventsWeighted = {m_weightsTreeReader, "totalEventsWeighted"};
  TTreeReaderValue<ULong64_t> totalEvents = {m_weightsTreeReader, "totalEvents"};
};

#endif // UPGRADEANALYSIS_UPGRADELOOP_H__
