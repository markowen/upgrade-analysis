#include "UpgradeAnalysis/ExampleAnalysis.h"
#include <iostream>
#include "TH1D.h"
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void ExampleAnalysis::DefineHistograms() {

  h_njets = new TH1D("h_njets","h_njets",10,-0.5,9.5);
  h_nbjets = new TH1D("h_nbjets","h_nbjets",5,-0.5,4.5);
  h_jet_eta = new TH1D("h_jet_eta","h_jet_eta;Jets,#eta",100,-5.0,5.0);

  h_truth_t_y = new TH1D("h_truth_t_y","h_truth_t_y;Top y;Events",100,-5.0,5.0);
  h_truth_t_y_withreco = new TH1D("h_truth_t_y_withreco","h_truth_t_y_withreco;Top y;Events",100,-5.0,5.0);

  h_pdfweight = new TH1D("h_pdfweight","h_pdfweight",100,0.7,1.3);
}

void ExampleAnalysis::ProcessEvent(bool doReco) {
  
  if(doReco) { // this flag tells us if the event is in the reconstructed tree

    // Example of how to get a weight for CT14 PDF compared to the standard one used in the MC (NNPDF 3.0)
    double pdfweight = m_pdfReweighter.getReweightingFactor( PDFinfo_X1[0], PDFinfo_PDGID1[0], PDFinfo_X2[0], PDFinfo_PDGID2[0], PDFinfo_Q[0], PDFinfo_XF1[0], PDFinfo_XF2[0], CT14 );   
    h_pdfweight->Fill( pdfweight );

    // let's require 4 jets with |eta|<4.0
    int ngoodjets(0);
    for(auto jetit : tlv_jets) {
      if( fabs(jetit.Eta()) < 4.0 ) ++ngoodjets;
    }
    if(ngoodjets >= 4) {
      
      h_njets->Fill( tlv_jets.size() );
      int nbjets(0);
      // loop on jets and plot something
      for(unsigned int ij=0; ij<tlv_jets.size(); ++ij) {
	const TLorentzVector& jet = tlv_jets[ij];
	h_jet_eta->Fill( jet.Eta() );
	if(jet_istagged[ij]) ++nbjets;
      }
      h_nbjets->Fill(nbjets);
      // plot truth for events we reconstructed
      h_truth_t_y_withreco->Fill( tlv_truth_t.Rapidity() );
    }
  }//Reco

  // plot some truth stuff (available for all events)
  h_truth_t_y->Fill( tlv_truth_t.Rapidity() );
}
