#ifndef UPGRADEANALYSIS_EXAMPLEANALYSIS_H__
#define UPGRADEANALYSIS_EXAMPLEANALYSIS_H__

#include "UpgradeTree.h"
#include "PDFReweight.h"

#include "TH1.h"

class ExampleAnalysis : public UpgradeTree {
  
 public:
  
  ExampleAnalysis() { }
  ~ExampleAnalysis() { }
  
  virtual void ProcessEvent(bool doReco) ;
  
  virtual void DefineHistograms();

 protected:

  // Class that does PDF reweighting
  PDFReweight m_pdfReweighter;

  // Declare here histograms that we want to fill
  TH1* h_njets;
  TH1* h_nbjets;
  TH1* h_jet_eta;
  TH1* h_truth_t_y;
  TH1* h_truth_t_y_withreco;
  TH1* h_pdfweight;

};//ExampleAnalysis

#endif //UPGRADEANALYSIS_EXAMPLEANALYSIS_H__
