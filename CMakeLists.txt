cmake_minimum_required (VERSION 2.6)
SET(CMAKE_CXX_COMPILER g++)
SET(CMAKE_C_COMPILER gcc)

project (UpgradeAnalysis)

find_package( AnalysisBase )

# Turn this on if you want to debug the make commands
#set( CMAKE_VERBOSE_MAKEFILE on )

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
list(APPEND CMAKE_MODULE_PATH $ENV{ROOTSYS}/etc/cmake/)

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS RIO Core Hist MathCore Net Physics Tree TreePlayer)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
#include(${ROOT_USE_FILE})

#---Find LHAPDF
find_package( Lhapdf )

# Add main directory to the include path
include_directories(${PROJECT_SOURCE_DIR})

add_library(UpgradeAnalysis SHARED Root/UpgradeTree.cxx Root/UpgradeLoop.cxx Root/ExampleAnalysis.cxx Root/PDFReweight.cxx
        UpgradeAnalysis/UpgradeTree.h UpgradeAnalysis/UpgradeLoop.h UpgradeAnalysis/ExampleAnalysis.h UpgradeAnalysis/PDFReweight.h)
target_link_libraries(UpgradeAnalysis ${ROOT_LIBRARIES} ${LHAPDF_LIBRARIES})
target_include_directories(UpgradeAnalysis PUBLIC ${LHAPDF_INCLUDE_DIRS})
#target_include_directories(UpgradeAnalysis PUBLIC "${PROJECT_SOURCE_DIR}")
# This adds debug flags
target_compile_options(UpgradeAnalysis PRIVATE -g)

add_executable(upgrade-ana util/upgrade-ana.cxx)
target_compile_options(upgrade-ana PRIVATE -g)
target_link_libraries (upgrade-ana UpgradeAnalysis)
target_link_libraries(upgrade-ana ${ROOT_LIBRARIES})

