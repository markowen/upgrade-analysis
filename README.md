# upgrade-analysis

Small analysis package for analysing ntuples coming from running upgrade smearing functions in AnalysisTop.

# First time setup

To checkout and build the code:
```
asetup AnalysisBase,21.2.52
git clone https://gitlab.cern.ch/markowen/upgrade-analysis.git
cd upgrade-analysis
mkdir build
cd build
cmake ../
make
```
If 'asetup' is not working, then you need to make sure the ATLAS local setup is there, which can be done with:
```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
```
since you'll need to do that each time, perhaps easiest to put it in a 'ALRB.sh' file and then just 'source ./ALRB.sh' on login.

# How to run

To run the code:
```
asetup AnalysisBase,21.2.52
cd upgrade-analysis
cd build
make
./upgrade-ana
```
you need to re-run the make command after any changes you make locally.

# Developing

We don't generally put changes directly to master, so before
making changes you should create a new branch to work on:
```
git checkout -b my_nice_branch_name master
```
Now you make some changes and test them. Then you commit them to
git locally:
```
git add pathTo/aFileThatChanged.cxx
git add pathTo/anotherFileThatChanged.cxx
git commit -m "Nice log message"
```
And finally you can push the changes to the git repository:
```
git push origin my_nice_branch_name
```
Then if you happen to need to come back to a different working
area (e.g. lxplus rather than ppe), you can always checkout the branch
like
```
git checkout my_nice_branch_name
```


If you forgot the name of your branch, you can see it by doing:
```
git branch
```
The branch name with the * is the one you are working on.

After the push to the remote repository, git will automatically build
the latest version of the branch, you can see the results here:
https://gitlab.cern.ch/markowen/upgrade-analysis/pipelines. If the build fails,
you should fix it!

Once you have some development that is working well, you can
merge it with the master branch (so other people can access it).
To do this, you go to the webpage (https://gitlab.cern.ch/markowen/upgrade-analysis)
and click 'New merge request' (you may have to go first to the 'Merge Requests'
tab). The most important thing to check in the form is that the origin
branch is set to the one you have been working on and the destination
branch is set to master. Once the request is made, then it goes for approval
before it is actually merged into the master branch.
