#ifndef UPGRADEANALYSIS_UPGRADETREE_H__
#define UPGRADEANALYSIS_UPGRADETREE_H__

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TLorentzVector.h>
#include <TRandom3.h>

#include <vector>

class UpgradeTree {

 public :
  UpgradeTree() ;
  ~UpgradeTree() ;

  /// Function to set the TTrees we need
  void SetTrees(TTree* upgradeTree, TTree* truthTree);

  /// This function creates the output file & books the histograms
  void CreateHistograms();

  /// This function runs the loop on the current loaded TTree
  void LoopOnCurrentTrees();
  
  /// Function called for each event, to be overridden in derived class
  virtual void ProcessEvent(bool doReco) = 0;

  /// Function to define your histograms, to be overridden in derived class
  virtual void DefineHistograms() = 0;
  
  /// Function sets up the TLorentzVectors from the input objects
  void LoadObjects(bool doReco);

  /// Access output file
  inline TFile* GetOutputFile() {return fout;}

 protected :

  std::vector<TLorentzVector> tlv_leptons; //! The selected leptona
  std::vector<TLorentzVector> tlv_jets; //! Vector of jets
  std::vector<int> jet_istagged; //! whether each jet is b-tagged or not

  float metx; // MET component in x-direction
  float mety; // MET component in y-direction

  TLorentzVector tlv_truth_t; //! truth top
  TLorentzVector tlv_truth_tbar; //! truth anti-top

  TFile* fout; //! Output file
  
  TRandom3 myrand;

   TTreeReader     fReaderUpgrade;  //!the tree reader for upgrade tree
   TTreeReader     fReaderTruth;  //!the tree reader for truth
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Float_t> weight_mc = {fReaderUpgrade, "weight_mc"};
   TTreeReaderValue<UInt_t> mcChannelNumber = {fReaderUpgrade, "mcChannelNumber"};
   TTreeReaderValue<Float_t> mu = {fReaderUpgrade, "mu"};
   TTreeReaderArray<float> el_pt = {fReaderUpgrade, "el_pt"};
   TTreeReaderArray<float> el_eta = {fReaderUpgrade, "el_eta"};
   TTreeReaderArray<float> el_phi = {fReaderUpgrade, "el_phi"};
   TTreeReaderArray<float> el_e = {fReaderUpgrade, "el_e"};
   TTreeReaderArray<float> el_charge = {fReaderUpgrade, "el_charge"};
   TTreeReaderArray<float> mu_pt = {fReaderUpgrade, "mu_pt"};
   TTreeReaderArray<float> mu_eta = {fReaderUpgrade, "mu_eta"};
   TTreeReaderArray<float> mu_phi = {fReaderUpgrade, "mu_phi"};
   TTreeReaderArray<float> mu_e = {fReaderUpgrade, "mu_e"};
   TTreeReaderArray<float> mu_charge = {fReaderUpgrade, "mu_charge"};
   TTreeReaderArray<float> jet_pt = {fReaderUpgrade, "jet_pt"};
   TTreeReaderArray<float> jet_eta = {fReaderUpgrade, "jet_eta"};
   TTreeReaderArray<float> jet_phi = {fReaderUpgrade, "jet_phi"};
   TTreeReaderArray<float> jet_e = {fReaderUpgrade, "jet_e"};
   TTreeReaderArray<float> jet_mv1eff = {fReaderUpgrade, "jet_mv1eff"};
   TTreeReaderArray<float> jet_isPileup = {fReaderUpgrade, "jet_isPileup"};
   TTreeReaderArray<int> jet_nGhosts_bHadron = {fReaderUpgrade, "jet_nGhosts_bHadron"};
   TTreeReaderArray<int> jet_nGhosts_cHadron = {fReaderUpgrade, "jet_nGhosts_cHadron"};
   TTreeReaderValue<Float_t> met_met = {fReaderUpgrade, "met_met"};
   TTreeReaderValue<Float_t> met_phi = {fReaderUpgrade, "met_phi"};


   // Variables in MC truth tree
   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<ULong64_t> eventNumber = {fReaderTruth, "eventNumber"};
   TTreeReaderValue<UInt_t> runNumber = {fReaderTruth, "runNumber"};
   TTreeReaderArray<float> PDFinfo_X1 = {fReaderTruth, "PDFinfo_X1"};
   TTreeReaderArray<float> PDFinfo_X2 = {fReaderTruth, "PDFinfo_X2"};
   TTreeReaderArray<int> PDFinfo_PDGID1 = {fReaderTruth, "PDFinfo_PDGID1"};
   TTreeReaderArray<int> PDFinfo_PDGID2 = {fReaderTruth, "PDFinfo_PDGID2"};
   TTreeReaderArray<float> PDFinfo_Q = {fReaderTruth, "PDFinfo_Q"};
   TTreeReaderArray<float> PDFinfo_XF1 = {fReaderTruth, "PDFinfo_XF1"};
   TTreeReaderArray<float> PDFinfo_XF2 = {fReaderTruth, "PDFinfo_XF2"};
   TTreeReaderValue<Int_t> MC_Wdecay2_from_t_pdgId = {fReaderTruth, "MC_Wdecay2_from_t_pdgId"};
   TTreeReaderValue<Int_t> MC_Wdecay1_from_tbar_pdgId = {fReaderTruth, "MC_Wdecay1_from_tbar_pdgId"};
   TTreeReaderValue<Int_t> MC_Wdecay1_from_t_pdgId = {fReaderTruth, "MC_Wdecay1_from_t_pdgId"};
   TTreeReaderValue<Int_t> MC_Wdecay2_from_tbar_pdgId = {fReaderTruth, "MC_Wdecay2_from_tbar_pdgId"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_t_eta = {fReaderTruth, "MC_Wdecay2_from_t_eta"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_t_pt = {fReaderTruth, "MC_Wdecay2_from_t_pt"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_tbar_eta = {fReaderTruth, "MC_Wdecay1_from_tbar_eta"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_tbar_pt = {fReaderTruth, "MC_Wdecay1_from_tbar_pt"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_tbar_m = {fReaderTruth, "MC_Wdecay1_from_tbar_m"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_t_phi = {fReaderTruth, "MC_Wdecay1_from_t_phi"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_t_pt = {fReaderTruth, "MC_Wdecay1_from_t_pt"};
   TTreeReaderValue<Float_t> MC_b_from_tbar_m = {fReaderTruth, "MC_b_from_tbar_m"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_t_m = {fReaderTruth, "MC_Wdecay2_from_t_m"};
   TTreeReaderValue<Float_t> MC_b_from_t_phi = {fReaderTruth, "MC_b_from_t_phi"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_SC_pt = {fReaderTruth, "MC_t_afterFSR_SC_pt"};
   TTreeReaderValue<Float_t> MC_W_from_t_pt = {fReaderTruth, "MC_W_from_t_pt"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_m = {fReaderTruth, "MC_t_afterFSR_m"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_pt = {fReaderTruth, "MC_t_afterFSR_pt"};
   TTreeReaderValue<Float_t> MC_t_beforeFSR_eta = {fReaderTruth, "MC_t_beforeFSR_eta"};
   TTreeReaderValue<Float_t> MC_ttbar_beforeFSR_eta = {fReaderTruth, "MC_ttbar_beforeFSR_eta"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_tbar_phi = {fReaderTruth, "MC_Wdecay2_from_tbar_phi"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_SC_phi = {fReaderTruth, "MC_t_afterFSR_SC_phi"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_tbar_eta = {fReaderTruth, "MC_Wdecay2_from_tbar_eta"};
   TTreeReaderValue<Float_t> MC_W_from_tbar_eta = {fReaderTruth, "MC_W_from_tbar_eta"};
   TTreeReaderValue<Float_t> MC_b_from_tbar_eta = {fReaderTruth, "MC_b_from_tbar_eta"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_tbar_pt = {fReaderTruth, "MC_Wdecay2_from_tbar_pt"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_tbar_phi = {fReaderTruth, "MC_Wdecay1_from_tbar_phi"};
   TTreeReaderValue<Float_t> MC_t_beforeFSR_phi = {fReaderTruth, "MC_t_beforeFSR_phi"};
   TTreeReaderValue<Float_t> MC_ttbar_beforeFSR_m = {fReaderTruth, "MC_ttbar_beforeFSR_m"};
   TTreeReaderValue<Float_t> MC_ttbar_beforeFSR_pt = {fReaderTruth, "MC_ttbar_beforeFSR_pt"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_eta = {fReaderTruth, "MC_t_afterFSR_eta"};
   TTreeReaderValue<Float_t> MC_tbar_beforeFSR_m = {fReaderTruth, "MC_tbar_beforeFSR_m"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_SC_eta = {fReaderTruth, "MC_t_afterFSR_SC_eta"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_SC_m = {fReaderTruth, "MC_t_afterFSR_SC_m"};
   TTreeReaderValue<Float_t> MC_t_beforeFSR_m = {fReaderTruth, "MC_t_beforeFSR_m"};
   TTreeReaderValue<Float_t> MC_ttbar_afterFSR_phi = {fReaderTruth, "MC_ttbar_afterFSR_phi"};
   TTreeReaderValue<Float_t> MC_ttbar_beforeFSR_phi = {fReaderTruth, "MC_ttbar_beforeFSR_phi"};
   TTreeReaderValue<Float_t> MC_ttbar_afterFSR_eta = {fReaderTruth, "MC_ttbar_afterFSR_eta"};
   TTreeReaderValue<Float_t> MC_ttbar_afterFSR_m = {fReaderTruth, "MC_ttbar_afterFSR_m"};
   TTreeReaderValue<Float_t> MC_t_afterFSR_phi = {fReaderTruth, "MC_t_afterFSR_phi"};
   TTreeReaderValue<Float_t> MC_ttbar_afterFSR_pt = {fReaderTruth, "MC_ttbar_afterFSR_pt"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_tbar_m = {fReaderTruth, "MC_Wdecay2_from_tbar_m"};
   TTreeReaderValue<Float_t> MC_tbar_beforeFSR_eta = {fReaderTruth, "MC_tbar_beforeFSR_eta"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_m = {fReaderTruth, "MC_tbar_afterFSR_m"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_t_m = {fReaderTruth, "MC_Wdecay1_from_t_m"};
   TTreeReaderValue<Float_t> MC_tbar_beforeFSR_phi = {fReaderTruth, "MC_tbar_beforeFSR_phi"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_SC_phi = {fReaderTruth, "MC_tbar_afterFSR_SC_phi"};
   TTreeReaderValue<Float_t> MC_b_from_tbar_pt = {fReaderTruth, "MC_b_from_tbar_pt"};
   TTreeReaderValue<Float_t> MC_W_from_t_eta = {fReaderTruth, "MC_W_from_t_eta"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_pt = {fReaderTruth, "MC_tbar_afterFSR_pt"};
   TTreeReaderValue<Float_t> MC_b_from_t_m = {fReaderTruth, "MC_b_from_t_m"};
   TTreeReaderValue<Float_t> MC_tbar_beforeFSR_pt = {fReaderTruth, "MC_tbar_beforeFSR_pt"};
   TTreeReaderValue<Float_t> MC_W_from_tbar_pt = {fReaderTruth, "MC_W_from_tbar_pt"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_eta = {fReaderTruth, "MC_tbar_afterFSR_eta"};
   TTreeReaderValue<Float_t> MC_Wdecay1_from_t_eta = {fReaderTruth, "MC_Wdecay1_from_t_eta"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_SC_m = {fReaderTruth, "MC_tbar_afterFSR_SC_m"};
   TTreeReaderValue<Float_t> MC_W_from_t_m = {fReaderTruth, "MC_W_from_t_m"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_SC_pt = {fReaderTruth, "MC_tbar_afterFSR_SC_pt"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_phi = {fReaderTruth, "MC_tbar_afterFSR_phi"};
   TTreeReaderValue<Float_t> MC_b_from_t_eta = {fReaderTruth, "MC_b_from_t_eta"};
   TTreeReaderValue<Float_t> MC_b_from_tbar_phi = {fReaderTruth, "MC_b_from_tbar_phi"};
   TTreeReaderValue<Float_t> MC_tbar_afterFSR_SC_eta = {fReaderTruth, "MC_tbar_afterFSR_SC_eta"};
   TTreeReaderValue<Float_t> MC_W_from_t_phi = {fReaderTruth, "MC_W_from_t_phi"};
   TTreeReaderValue<Float_t> MC_Wdecay2_from_t_phi = {fReaderTruth, "MC_Wdecay2_from_t_phi"};
   TTreeReaderValue<Float_t> MC_W_from_tbar_m = {fReaderTruth, "MC_W_from_tbar_m"};
   TTreeReaderValue<Float_t> MC_t_beforeFSR_pt = {fReaderTruth, "MC_t_beforeFSR_pt"};
   TTreeReaderValue<Float_t> MC_W_from_tbar_phi = {fReaderTruth, "MC_W_from_tbar_phi"};
   TTreeReaderValue<Float_t> MC_b_from_t_pt = {fReaderTruth, "MC_b_from_t_pt"};
}; // class UpgradeTree

#endif //UPGRADEANALYSIS_UPGRADETREE_H__
