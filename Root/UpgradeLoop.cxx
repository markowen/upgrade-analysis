#include "UpgradeAnalysis/UpgradeLoop.h"
#include <iostream>

using std::cout;
using std::endl;

UpgradeLoop::UpgradeLoop(const std::vector<std::string>& filesToProc) :
  m_upgradeTree(0)
{
  m_filesToProc = filesToProc;
}

UpgradeLoop::~UpgradeLoop() {

}

void UpgradeLoop::SetAnalysisObject( UpgradeTree* anaobj ) {
  m_upgradeTree = anaobj;
}

int UpgradeLoop::RunLoop() {

  // Loop on files
  for(auto fileName : m_filesToProc) {
    cout << "Processing " << fileName.c_str() << endl;
    // Open file
    TFile* fin = TFile::Open(fileName.c_str(),"READ");
    if(!fin or fin->IsZombie()) {
      cout << "ERROR with file " << fileName.c_str() << endl;
      return 1;
    }
    // Get Trees
    TTree* tup = dynamic_cast<TTree*>(fin->Get("upgrade"));
    TTree* tmc = dynamic_cast<TTree*>(fin->Get("truth"));
    if(!tup or !tmc) {
      cout << "ERROR with a tree in " << fileName.c_str() << endl;
      return 1;
    }
    m_upgradeTree->SetTrees(tup, tmc);

    // loop on events
    m_upgradeTree->LoopOnCurrentTrees();
    
    // Get the sum of weights tree from input file
    TTree* tw = dynamic_cast<TTree*>(fin->Get("sumWeights"));
    m_weightsTreeReader.SetTree(tw);
    
    // after here we can loop on the sumWeights tree and sum up how many events were
    // processed according to the totalEvents and totalEventsWeighted branches
    for(unsigned int i=0; i<tw->GetEntries(); ++i) {
      m_weightsTreeReader.SetEntry(i);
      std::cout << "totalEventsWeighted = " << *totalEventsWeighted << std::endl;
    }

    // output file can be accessed like:
    //TFile* fout = m_upgradeTree->GetOutputFile();

    // close file
    fin->Close();
    delete fin;
  }//loop on files
  return 0;
}
