#include "UpgradeAnalysis/UpgradeLoop.h"
#include "UpgradeAnalysis/ExampleAnalysis.h"

#include <vector>
#include <string>
#include <fstream>

int main() {

  std::vector<std::string> fileNames;

  std::ifstream infile("/nfs/atlas/ttbar/UpgradeNtuples/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/410401.list");
  std::string line;
  while (std::getline(infile, line)) {
    fileNames.push_back(line);
    std::cout << "Will process " << line.c_str() << std::endl;
  }

  ExampleAnalysis myAnalysis;
  myAnalysis.CreateHistograms();

  UpgradeLoop theLooper(fileNames);

  theLooper.SetAnalysisObject(&myAnalysis);

  theLooper.RunLoop();

  return 0;
}
